// routing
import riot from "riot";
import route from "riot-route";

route.base("#/");
route.start(true);

const scrollTo = (elementOffset) => {
  const navOffset = document.innerWidth < 1024 
    ? document.querySelector('.sidebar').offsetHeight
    : 0;
  
  setTimeout(function(){
    window.scroll({
      top: elementOffset - navOffset, 
      left: 0, 
      behavior: 'smooth'
    });
  }, 100);
}

route("/", function() {
  const elementOffset = document.getElementById("home").offsetTop;
  scrollTo(elementOffset);
});

route("/education", function() {
  const elementOffset = document.getElementById("education").offsetTop;
  scrollTo(elementOffset);
});

route("/experience", function() {
  const elementOffset = document.getElementById("experience").offsetTop;
  scrollTo(elementOffset);
});

route("/portfolio", function() {
  const elementOffset = document.getElementById("portfolio").offsetTop;
  scrollTo(elementOffset);
});

route("/skills", function() {
  const elementOffset = document.getElementById("skills").offsetTop;
  scrollTo(elementOffset);
});

route("/contact", function() {
  const elementOffset = document.getElementById("contact").offsetTop;
  scrollTo(elementOffset);
});
