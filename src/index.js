import '@riotjs/hot-reload'
import "../src/global/config/router";
import { component } from 'riot';
import App from './pages/app/app.riot';

component(App)(document.getElementById('view'))