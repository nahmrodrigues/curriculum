# Curriculum Page

Try in [https://nahmrodrigues.gitlab.io/curriculum/](https://nahmrodrigues.gitlab.io/curriculum/)

## Table of Contents

1. [Stack Used](#stack-used)
2. [Files and Folder Structure](#files-and-folder-structure)


## Stack Used

| Name | Link | Description |
| -- | -- | -- |
| <a href="https://riot.js.org/"><img height="30px" src="https://riot.js.org/img/logo/riot-logo.svg"></a> | https://riot.js.org/| Component-based UI library |
| <a href="https://babeljs.io/"><img height="30px" src="https://d33wubrfki0l68.cloudfront.net/7a197cfe44548cc1a3f581152af70a3051e11671/78df8/img/babel.svg"></a> | https://babeljs.io/ | A JavaScript compiler |
| <a href="https://sass-lang.com/"><img height="30px" src="https://sass-lang.com/assets/img/logos/logo-b6e1ef6e.svg"></a> | https://sass-lang.com | CSS with superpowers |
| <a href="https://webpack.js.org/"><img height="30px" src="https://webpack.js.org/icon-square-small.85ba630cf0c5f29ae3e3.svg"></a> | https://webpack.js.org/ | A static module bundler for modern JavaScript applications |


## Files and Folder Structure

```
.
├── src/
|   ├── assets/  
│   |   ├── fonts/
│   |   ├── img/
|   |   ├── svg/
│   │   └── (...)
│   ├── components/
│   │   ├── component-1/
│   │   │   ├── component-1.scss
│   │   │   └── component-1.riot
│   │   ├── component-2/
│   │   │   └── (...)
|   ├── global/
│   │   ├── config/
│   │   │   └── (...)
│   │   ├── styles/
│   │   │   ├── variables.scss
│   │   │   └── (...)
│   ├── pages/
│   │   ├── page-1/
│   │   │   ├── page-1.scss
│   │   │   └── page-1.riot
│   │   ├── page-2/
│   │   │   └── (...)
│   ├── index.html
│   ├── index.js
|   └── (...)
├── configuration-file-1
├── configuration-file-2
└── (...)

```

**Explaning...**

The idea is the components and pages being totally app independent.

-   `.scss`: Each component or page, if needs, will have it's own`.scss` and the configurations

```
.component-container {
    background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)),
        url("../../img/component-background-image.jpg");
    background-size: auto;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
}
```

-   `.riot`: Each component has a `.riot` file, which contains the component code. The Riot syntax is written into it, giving the component form and logic.

```
<component>
    <div class="component-container">
        This is the {state.name} component.
    </div>
    <script>
        export default {
            onMounted() {
                this.update({
                    name: "Test"
                });
            }
        };
    </script>
</component>
```

- The 'assets/' folder will contain static project files such as images, fonts and other types of static assets. It may or may not be necessary.

- The 'components/' folder will contain the application components, that can be used inside another components or pages.

- The 'global/' folder will contain application global files. It can have as many as sub-folders needed. The folder 'config/' will contain global configuration files, such as routing and store, and 'styles/' folder will contain the global styles files.

- The 'pages/' folder will contain the application pages.