const path = require("path"); // native node package

module.exports = {
    from: "./src",
    ext: "riot",
    to: path.resolve(__dirname, "build") + "/components.js"
};

// __dirname is a node variable with the path of the actual folder
